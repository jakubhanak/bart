<?php

use Faker\Generator as Faker;

$factory->define(App\Gallery::class, function (Faker $faker) {
    $name =  $faker->sentence(2);
    $path =  kebab_case($name);
    return [
        'name' => $name,
        'path' => $path
    ];
});
