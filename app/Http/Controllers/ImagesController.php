<?php

namespace App\Http\Controllers;

use App\Image;
use Illuminate\Http\Request;
use Image as Resizer;

class ImagesController extends Controller
{
    /**
     * Show resized image
     *
     * @param int $w
     * @param int $h
     * @param string $project
     * @param string $image
     * @return void
     */
    public function index($w, $h, $project, $image)
    {
        $image = Image::where('fullpath', $project . '/' .$image)->first();
        if (! $image) {
            return response()->json([
                'error' => [
                    'Image not found'
                ]
                ], 404);
        }

        if ($w == 0) {
            $w = null;
        }
        if ($h == 0) {
            $h = null;
        }

        $img = Resizer::make('../storage/app/uploads/'.$image->path)->resize($w, $h, function ($constraint) use ($w,$h) {
            if (is_null($w) || is_null($h)) {
                $constraint->aspectRatio();
            }
        });

        return $img->response('jpg');
    }
}
