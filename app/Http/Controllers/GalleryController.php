<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;

class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $galleries =  Gallery::select('name', 'path')->get();

        return response()->json([
            'galleries' => $galleries->toArray()
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|regex:/^(?!.*\/).*$/'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => [
                    'message' => 'Name is required and does not contains /'
                ]
            ], 400);
        }

        $validator = Validator::make($request->all(), [
            'name' => 'unique:galleries'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => [
                    'message' => 'Gallery with this name already exists'
                ]
            ], 409);
        }
        
        return Gallery::create([
            'name' => $request->name,
            'path' => kebab_case($request->name)
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function show($path)
    {
        $gallery = Gallery::where('path', $path)->first();

        if (! $gallery) {
            return response()->json([
                'error' => [
                    'message' => 'Selected gallery does not exists'
                ]
            ], 404);
        }

        return response()->json([
            'gallery' => $gallery->only('name', 'path'),
            'images' => $gallery->images->map(function ($image) {
                return [
                    'path' => $image->path,
                    'fullpath' => $image->fullpath,
                    'name' => $image->name,
                    'modified' => $image->created_at->format('Y-m-d H:i:s')
                ];
            })
        ], 200);
    }

    /**
     * Post method to upload images
     *
     * @param Request $request
     * @param string $path
     * @return void
     */
    public function uploadImage(Request $request, $path)
    {
        $gallery = Gallery::where('path', $path)->first();

        if (! $gallery) {
            return response()->json([
                'error' => [
                    'message' => 'Selected gallery does not exists'
                ]
            ], 404);
        }

        foreach ($request->image as $image) {
            $images[] = $this->processImage($image, $gallery);
        }

        return response()->json([
            'uploaded' => [
                $images
            ]
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function destroy($path)
    {
        $gallery = Gallery::where('path', $path)->first();

        if (! $gallery) {
            return response()->json([
                'error' => [
                    'message' => 'Selected gallery does not exists'
                ]
            ], 404);
        }

        $gallery->delete();

        return response()->json([
            'success' => [
                'message' => 'Gallery was successfully deleted'
            ]
        ], 200);
    }

    /**
     * Process one image
     *
     * @param file $image
     * @param model $gallery
     * @return mixed
     */
    protected function processImage($image, $gallery)
    {
        $validator = Validator::make(['image' => $image], [
            'image' => 'required|file|mimes:jpeg,jpg'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => [
                    'message' => 'Image not found or not valid'
                ]
            ], 400);
        }

        $name = $this->realNameWithoutExtension($image);
        $ext = $image->getClientOriginalExtension();

        $checkedName = $this->getImageNameToStore($name, $ext);

        $image->storeAs('uploads', $checkedName. '.' . $ext);

        $fullpath = $gallery->path. '/' . kebab_case($checkedName) . '.' . $ext;

        return $image = Image::create([
            'path' => $checkedName . '.' .$ext,
            'fullpath' => $fullpath,
            'name' => ucfirst($name),
            'gallery_id' => $gallery->id
        ]);
    }

    /**
     * Helper method to get the real name of file
     *
     * @param file $image
     * @return string
     */
    protected function realNameWithoutExtension($image)
    {
        return basename($image->getClientOriginalName(), '.'.$image->getClientOriginalExtension());
    }

    /**
     * Check if image with given parameters exists
     *
     * @param string $name
     * @param string $ext
     * @return string
     */
    protected function getImageNameToStore($name, $ext)
    {
        $count = 0;
        $checkedName = $name;
        while (Storage::exists('uploads/'.$checkedName. '.' . $ext)) {
            $checkedName = $name . $count++;
        }

        return $checkedName;
    }
}
