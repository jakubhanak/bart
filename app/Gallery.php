<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $fillable = ['name', 'path'];

    public function getRouteKeyName()
    {
        return 'path';
    }

    public function images()
    {
        return $this->hasMany(Image::class);
    }
}
