<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GalleryTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_get_response_with_all_galleries()
    {
        $gallery1 = factory('App\Gallery')->create();
        $gallery2 = factory('App\Gallery')->create();
        
        $this->get('/api/gallery')
            ->assertSee($gallery1->name)
            ->assertSee($gallery2->name)
            ->assertSee($gallery1->path)
            ->assertSee($gallery2->path);
    }

    /** @test */
    public function user_can_create_gallery()
    {
        $this->withExceptionHandling();

        $gallerySuccess = ['name' => 'Test Gallery'];
        $gallerySuccessPath = kebab_case($gallerySuccess['name']);
        $galleryError = ['name' => 'Test/Gallery'];
        $galleryErrorPath = kebab_case($galleryError['name']);
        

        $this->post('/api/gallery', $gallerySuccess)
            ->assertStatus(201)
            ->assertSee($gallerySuccess['name'])
            ->assertSee($gallerySuccessPath);
        
        $this->post('/api/gallery', $galleryError)
            ->assertStatus(400);
        
        $this->post('/api/gallery', $gallerySuccess)
            ->assertStatus(409);
    }
}
